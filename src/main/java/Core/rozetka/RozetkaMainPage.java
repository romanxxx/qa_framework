package Core.rozetka;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RozetkaMainPage {
    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    public static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    @FindBy(xpath = "//input[@class='rz-header-search-input-text passive']")
    private WebElement inputSearch;

    public RozetkaMainPage(final WebDriver webDriver) {
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public RozetkaSearchResultPage inputSearchGoodsName(final String text) {
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        inputSearch.sendKeys(text, Keys.ENTER);
        return new RozetkaSearchResultPage(webDriver);
    }

}

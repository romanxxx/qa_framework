package Core.rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class RozetkaWishlistPage {
    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    public static final Long ELEMENT_WAIT_TIME_OUT = 30L;


    public RozetkaWishlistPage(final WebDriver webDriver){
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver,this);
    }
    @FindBy(xpath = "//input[@placeholder='Поиск']")
    private WebElement inputSearch;
    @FindBy(xpath = "//a[contains(.,'Панель Speck Presidio Grip для Apple iPhone Xs Max Black')]")
    private WebElement panelPresidio;



    @FindBys({

            @FindBy(xpath = "//a[@class=\"novisited g-title-link wishlist-g-i-title-link\"]")
    })
    private List<WebElement> actualResultWishList;

    public String getFirstResultWishListText() {
        final Integer firstResultWishListText = 0;
        return actualResultWishList.get(firstResultWishListText).getText();
    }

}

package Core.rozetka;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RozetkaSearchResultPage {
    private static final String GOODS = "//a[contains(.,%s)]";
    @FindBy(xpath = "//a[@class='detail-wishlist-link novisited sprite-side']")
    private WebElement buttonToWishList;
    @FindBy(xpath = "//input[@class='input-text wishlists-i-input' and @name='login']")
    private WebElement inputEmail;
    @FindBy(xpath = "//button[@class='btn-link-i']")
    private WebElement buttonSaveEmail;

    @FindBy(xpath = "//input[@name='fio']")
    private WebElement inputFIO;
    @FindBy(xpath = "//button[@name='confirm_signup_submit']")
    private WebElement buttonSaveFIO;
    @FindBy(xpath = "//a[@class='confirm-email-popup-i-close novisited']")
    private WebElement buttonXlosePopUp;
    @FindBy(xpath = "(//div[@id='wishlist'])//a")
    private WebElement wishList;


    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    public static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    public RozetkaSearchResultPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    /**
     * @param Принимает страницу с результатами поиска
     * @return Новую страницу с выбранным товаром
     */
    public RozetkaSearchResultPage selectGood(final String Good) {
        final String goodLocator = String.format(GOODS, Good);
        final WebElement requiredGood = webDriver.findElement(By.xpath(goodLocator));
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        requiredGood.click();
        return new RozetkaSearchResultPage(webDriver);
    }

    public void clickAddToWishList() {

        buttonToWishList.click();

    }

    public void inputEmail(String text) {

        inputEmail.sendKeys(text);
        buttonSaveEmail.click();

    }

    public void inputFIO(String text) {

        inputFIO.sendKeys(text);
        buttonSaveFIO.click();

    }

    public void popupClose() {
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        buttonXlosePopUp.click();

    }

    public RozetkaWishlistPage clickOpenWishList() {
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        wishList.click();
        return new RozetkaWishlistPage(webDriver);
    }

}


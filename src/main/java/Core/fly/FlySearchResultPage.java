package Core.fly;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class FlySearchResultPage {

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 50L;


    @FindBys({

            @FindBy(xpath = "//a[@class = 'btn btn-primary btn-lg btn-block search_btn_big toggleShowHideDetails']")

    })
    private List<WebElement> ButtonsMoreDetails;

    @FindBy(xpath = "//div[@id='notLoggedInDiv']")
    private WebElement TimeOutElement;
    @FindBys({

            @FindBy(xpath = "(//div[@class='timing_belt'])//h3[contains(text(),'Depart')]")

    })
    private List<WebElement> ActualResultDate;

    public FlySearchResultPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);
    }

    public void clickAllButtonMoreDetails() {

        webDriverWait.until(ExpectedConditions.elementToBeClickable(TimeOutElement));
        for (final WebElement element : ButtonsMoreDetails) {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();
        }
    }

    public List<String> getActualResultDate() {
        final List<String> dateItemsTextList = new ArrayList<String>();
        for (final WebElement element : ActualResultDate) {
            dateItemsTextList.add(element.getText());
        }
        return dateItemsTextList;
    }

}

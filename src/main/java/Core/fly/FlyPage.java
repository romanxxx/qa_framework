package Core.fly;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class FlyPage {
    int count;

    @FindBy(xpath = "//input[@id='searchinput-from']")
    private WebElement InputFrom;

    @FindBy(xpath = "//input[@id='searchinput-to']")
    private WebElement InputTo;

    @FindBy(xpath = "(//div[@class='calendar_selection_new_cal'])[1]")
    private WebElement ButtonDateFrom;


    @FindBy(xpath = "(//div[@id='ui-datepicker-div'])//a[contains(., 'Next')]")
    private WebElement ButtonNextMonth;

//    @FindBy(xpath = "//a[@class=ui-datepicker-next ui-corner-all]")
//    private WebElement ButtonNextDay;


    @FindBy(xpath = "")
    private WebElement Day;

    @FindBy(xpath = "//span[@class='ui-datepicker-month']")
    private WebElement Month;

    @FindBy(xpath = "//span[@class='ui-datepicker-year']")
    private WebElement Year;

    @FindBy(xpath = "//input[@id ='onewayButton']")
    private WebElement RadioButtonOneDay;

    @FindBys(
            @FindBy(xpath = "(//table[@class='ui-datepicker-calendar'])//a")
    )
    private List<WebElement> DaysOfMonth;
    private static final String CURRENT_MONTH = "//span[@class='ui-datepicker-month']";
    private static final String CURRENT_YEAR = "//span[@class='ui-datepicker-year']";
    private static final String CURRENT_DAYS = "(//table[@class='ui-datepicker-calendar'])//a[.='%s']";

    @FindBy(xpath = "//button[@id ='search-btn']")
    private WebElement ButtonSearch;

    @FindBys({

            @FindBy(xpath = "//*[contains(text(),'More Details')]")
    })

    private List<WebElement> searchResultLinks;


    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;
    private Integer expDate;

    public void setExpDate(Integer expDate) {
        this.expDate = expDate;
    }


    public FlyPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);

    }

    public void clickRadiobuttonOneDay() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(RadioButtonOneDay));
        RadioButtonOneDay.click();

    }


    public void typeFromText(final String text) {
        InputFrom.click();
        InputFrom.sendKeys(text);
        InputFrom.sendKeys(Keys.ENTER);

    }

    public void typeToText(final String text) {
        InputTo.click();
        InputTo.sendKeys(text, Keys.ENTER);

    }

    public void clickButtonDateFrom() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(ButtonDateFrom));
        ButtonDateFrom.click();

    }

    public void searchForMonthAndYear(final String month, final String year) {

        while (!webDriver.findElement(By.xpath(CURRENT_MONTH)).getText().equalsIgnoreCase(month) || !webDriver.findElement(By.xpath(CURRENT_YEAR)).getText().equalsIgnoreCase(year)) {

            webDriverWait.until(ExpectedConditions.elementToBeClickable(ButtonNextMonth)).click();
        }


    }

    public void selectDay(final String expectedDay) {
        final String fullDayLocator = String.format(CURRENT_DAYS, expectedDay);
        final WebElement requiredDay = webDriver.findElement(By.xpath(fullDayLocator));
        requiredDay.click();
    }


    public FlySearchResultPage clickButtonSearch() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(ButtonSearch));
        ButtonSearch.click();
        return new FlySearchResultPage(webDriver);
    }
}



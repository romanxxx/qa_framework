package Core.google;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleNamePage {

    @FindBy(xpath = "//Input[@id='lst-ib']")
    private WebElement searchInput;

    @FindBy(xpath = "//Input[@name='btnK']")
    private WebElement searchButton;

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    public GoogleNamePage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);

    }

    public GoogleSearchResultPage typeSearchText(final String text) {
        searchInput.sendKeys(text, Keys.ENTER);
        return new GoogleSearchResultPage(webDriver);
    }

    public String getExpectedOkButtonText() {
        return searchButton.getText();
    }

    public GoogleSearchResultPage clickSearchButton() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
        return new GoogleSearchResultPage(webDriver);
    }
}

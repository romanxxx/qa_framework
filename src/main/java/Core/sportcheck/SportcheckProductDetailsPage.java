package Core.sportcheck;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SportcheckProductDetailsPage {
    @FindBys({

            @FindBy(xpath = "(((//ul[@class='rfk_products'])[1])//div[@class='rfk_product'])//div[@class='rfk_name']")
    })
    private List<WebElement> searchResultLinks;

    @FindBy(xpath = "//h1[text()]")
    private WebElement checkLink;


    private WebDriver webDriver;


    public SportcheckProductDetailsPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public String getFirstSearchResultLinkText() {
        final Integer firstSearchResultListIndex = 0;
        return searchResultLinks.get(firstSearchResultListIndex).getText();
    }

    public String getCheckLink() {
        return checkLink.getText();
    }

}

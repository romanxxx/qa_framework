package Core.sportcheck;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SportcheckMainPage {

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    @FindBy(xpath = "(//input[@placeholder='Searching for something?'])[2]")
    private WebElement searchInput;

    @FindBy(xpath = "(//div[@class='rfk_product'])[1]")
    private WebElement firstLink;

    @FindBy(xpath = "//span[@class='menu-toggle__text']")
    private WebElement burgerButton;

    @FindBys(
            @FindBy(xpath = "//ul[@class='page-nav__list page-nav__list_short main-menu']//li[@class='page-nav__item']")
    )

    private List<WebElement> filterItems;
    @FindBy(xpath = "(//li[@class='rfk_product'])[1]//a")
    private WebElement firstPickedJustForYouItem;

    @FindBy(xpath = "(//div[@class='header-account__trigger'])[1]")
    private WebElement ButtonSign;

    @FindBy(xpath = "//a[@class='header-account__sign-in__register__link']")
    private WebElement ButtonRegisterNow;


    public SportcheckMainPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);

    }

    public void expandFilterBox() {
        burgerButton.click();
    }

    public List<String> getFilterItemsList() {
        final List<String> filterItemsTextList = new ArrayList<String>();
        for (final WebElement element : filterItems) {
            filterItemsTextList.add(element.getText());
        }
        return filterItemsTextList;
    }

    public SportcheckProductDetailsPage typeSearchText(final String text) {
        searchInput.sendKeys(text);
        return new SportcheckProductDetailsPage(webDriver);
    }


    public SportcheckProductDetailsPage clickFirstElement() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(firstLink));
        firstLink.click();
        return new SportcheckProductDetailsPage(webDriver);
    }

    public void clickSignButton() {
        ButtonSign.click();
    }

    public SportCheckSignAndRegister clickRegisterNowButton() {
        ButtonRegisterNow.click();
        return new SportCheckSignAndRegister(webDriver);
    }


    public ProductDetailsPage selectFirstElementFromThePickedJustForYouSection() {
        firstPickedJustForYouItem.click();
        return new ProductDetailsPage(webDriver);
    }
}


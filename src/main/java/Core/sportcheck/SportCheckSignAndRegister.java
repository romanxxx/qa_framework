package Core.sportcheck;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SportCheckSignAndRegister {
    @FindBys({

            @FindBy(xpath = "(//div[@class='modal__hide'])//input")
    })
    private List<WebElement> FormRegistration;


    @FindBy(xpath = "(//div[@class='modal__hide'])//input[1]")
    private WebElement FieldEmailAddress;

    @FindBy(xpath = "(//div[@class='modal__hide'])//input[2]")
    private WebElement FieldConfirmEmail;

    @FindBy(xpath = "(//div[@class='modal__hide'])//input[3]")
    private WebElement FieldPassword;

    @FindBy(xpath = "(//div[@class='modal__hide'])//input[4]")
    private WebElement FieldConfirmPassword;


    @FindBy(xpath = "//input[@class='card-number-form__number card-number-form__number_js']")
    private WebElement FieldYourCardNumber;


    @FindBy(xpath = "(//div[@class='signin-form__submit-wrap'])//input[@class='signin-form__submit button']")
    private WebElement ButtonSign;

    @FindBy(xpath = "//span[@for='login']")
    private WebElement GetTextTipEmail;

    @FindBy(xpath = "//span[@for='loginConfirmation']")
    private WebElement GetTextTipConfirmEmail;

    @FindBy(xpath = "//span[@for='password']")
    private WebElement GetTextTipPassword;

    @FindBy(xpath = "//span[@for='confirmPassword']")
    private WebElement GetTextTipConfirmPassword;


    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    public SportCheckSignAndRegister(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);
    }


    public SportCheckSignAndRegister enterTextIntoFieldEmail(final String text) {
        FieldEmailAddress.sendKeys(text, Keys.TAB);
        return new SportCheckSignAndRegister(webDriver);
    }

    public SportCheckSignAndRegister enterTextIntoFieldConfirmEmail(final String text) {
        FieldConfirmEmail.sendKeys(text, Keys.TAB);
        return new SportCheckSignAndRegister(webDriver);
    }

    public SportCheckSignAndRegister enterTextIntoFieldPassword(final String text) {
        FieldPassword.sendKeys(text, Keys.TAB);
        return new SportCheckSignAndRegister(webDriver);
    }

    public SportCheckSignAndRegister enterTextIntoFieldConfirmPassword(final String text) {
        FieldConfirmPassword.sendKeys(text, Keys.TAB);
        return new SportCheckSignAndRegister(webDriver);
    }

    public String getWarningEmailMessageText() {

        return GetTextTipEmail.getText();
    }

    public String getWarningConfirmEmailMessageText() {

        return GetTextTipConfirmEmail.getText();
    }

    public String getWarningPasswordMessageText() {

        return GetTextTipPassword.getText();
    }

    public String getWarningConfirmPasswordMessageText() {

        return GetTextTipConfirmPassword.getText();
    }

}

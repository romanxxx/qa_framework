package Core.booking;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class BookingSearchResultPage {


    @FindBys(
            @FindBy(xpath = "//a[contains(@class,' district_link')]")
    )
    private List<WebElement> ActualResultDestination;


    private WebDriver webDriver;
    private WebDriverWait webDriverWait;

    public BookingSearchResultPage(final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }


    public List<String> getActualResultDestination() {
        final List<String> desinationItemsTextList = new ArrayList<String>();
        for (final WebElement element : ActualResultDestination) {
            desinationItemsTextList.add(element.getText());
        }
        return desinationItemsTextList;
    }

}

package Core.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingMainPage {

    @FindBy(xpath = "//input[@id='ss']")
    private WebElement destination;

    @FindBy(xpath = "//img[@alt='Stockholm, Stockholm county, Sweden']")
    private WebElement destinationType;


    @FindBy(xpath = "//div[@data-calendar2-title='Check-in']")
    private WebElement openCalendar;

    @FindBy(xpath = "//label[@id='xp__guests__toggle']")
    private WebElement guest;

    @FindBy(xpath = "//button[@data-sb-id='main']")
    private WebElement searchButton;

    @FindBy(xpath = "//div[@data-bui-ref='calendar-next']")
    private WebElement nextDateButton;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchTextFieldInput;

    @FindBy(xpath = "//label[@id='xp__guests__toggle']")
    private WebElement buttonGuestsCount;

    @FindBy(xpath = "//select[@name = 'no_rooms']")
    private WebElement selectNumber;

    @FindBy(xpath = "//select[@name = 'group_adults']")
    private WebElement selectAdults;

    @FindBy(xpath = "//select[@name = 'group_children']")
    private WebElement selectGroupChildren;



    private static final String CURRENT_MONTH_AND_YEAR = "(//div[@class='bui-calendar__month'])[1]";
    private static final String CURRENT_DAYS = "//div[@class='bui-calendar__wrapper'][1]//td[@data-bui-ref='calendar-date' and contains(@data-date, '%s')]";
    //a[@href="#" and @class = "ui-state-default"]
    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    public BookingMainPage(final WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void searchForMonthAndYear(final String monthAndYear) {
        openCalendar.click();
        while (!webDriver.findElement(By.xpath(CURRENT_MONTH_AND_YEAR)).getText().equalsIgnoreCase(monthAndYear)) {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(nextDateButton)).click();
        }

    }

    public void selectDay(final String expectedDay) {
        final String fullDayLocator = String.format(CURRENT_DAYS, expectedDay);
        final WebElement requiredDay = webDriver.findElement(By.xpath(fullDayLocator));
        requiredDay.click();
    }

    public void typeFromDestination(final String text) {
        destination.click();
        destination.sendKeys(text);
        destination.sendKeys(Keys.ARROW_DOWN);
        destinationType.click();

    }
    public void clickButtonGuestsCount(){
        webDriverWait.until(ExpectedConditions.elementToBeClickable(buttonGuestsCount));
        buttonGuestsCount.click();
    }
    public void setSelectNumberRooms(final Integer numberSizeIndex) {
        webDriverWait.until(ExpectedConditions.visibilityOf(selectNumber));
        final Select sizeSelect = new Select(selectNumber);
        sizeSelect.selectByIndex(numberSizeIndex);
    }
    public void setSelectAdultsCount(final Integer adultsCount) {
        webDriverWait.until(ExpectedConditions.visibilityOf(selectAdults));
        final Select sizeSelect = new Select(selectAdults);
        sizeSelect.selectByIndex(adultsCount);
    }
    public void setSelectGroupChildren(final Integer groupChildrenCount) {
        webDriverWait.until(ExpectedConditions.visibilityOf(selectGroupChildren));
        final Select sizeSelect = new Select(selectGroupChildren);
        sizeSelect.selectByIndex(groupChildrenCount);
    }
    public void clickFakeSearchButton(){
        webDriverWait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
    }
    public BookingSearchResultPage clickButtonSearch() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
        return new BookingSearchResultPage(webDriver);
    }

}

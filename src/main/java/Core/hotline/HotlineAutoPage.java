package Core.hotline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class HotlineAutoPage {

    @FindBy(xpath = "//li[@class ='level-1 auto']")
    private WebElement auto;

    @FindBys({

            @FindBy(xpath = "((//ul[@class='bricks cell-list'])//div[@class ='bricks-item'])//span[@class='h4']")
    })
    private List<WebElement> autoSubElements;

    private WebDriver webDriver;
    private WebDriverWait webDriverWait;
    private static final Long ELEMENT_WAIT_TIME_OUT = 30L;

    public HotlineAutoPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, ELEMENT_WAIT_TIME_OUT);
        PageFactory.initElements(webDriver, this);
    }

    public void clickAutoCategory() {
        auto.click();
    }

    public List<String> getautoSubElementsList() {
        final List<String> filterItemsTextList = new ArrayList<String>();
        for (final WebElement element : autoSubElements) {
            filterItemsTextList.add(element.getText());
        }
        return filterItemsTextList;
    }
}

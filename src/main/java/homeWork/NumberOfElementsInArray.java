package homeWork;

import java.util.*;


public class NumberOfElementsInArray {
    public static void main(String[] args) {

        int[] anyArray = {1, 2, 2, 3, 5, 6, 6};
        Arrays.sort(anyArray);


        HashMap hash = new HashMap();

        for (int i = 0; i < anyArray.length; i++) {
            Integer count = (Integer) hash.get(anyArray[i]);
            hash.put(anyArray[i], count == null ? 1 : count + 1);
        }
        System.out.println(hash);
    }
}








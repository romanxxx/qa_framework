package homeWork;

import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Please type a word");

        String anyText = in.next();

        if ((anyText.length() % 2) == 1) {
            int count = (anyText.length()) / 2 + 1;
            StringBuffer buffer = new StringBuffer(anyText.substring(count, anyText.length()));
            String reverse = buffer.reverse().toString();

            if (reverse.equalsIgnoreCase(anyText.substring(0, count - 1))) {
                System.out.println("This is Palindrome");

            } else {
                System.out.println("This is not a palindrome");
            }
        } else {
            int count = (anyText.length()) / 2;

            StringBuffer buffer = new StringBuffer(anyText.substring(count, anyText.length()));
            String reverse = buffer.reverse().toString();
            if (reverse.equalsIgnoreCase(anyText.substring(0, count))) {
                System.out.println("This is Palindrome");
            } else {
                System.out.println("This is not a palindrome");
            }
        }


    }

}

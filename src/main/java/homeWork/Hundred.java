package homeWork;

import java.util.Scanner;

public class Hundred {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please type a number");
        Integer inputNumber = in.nextInt();
        String shortNumber = inputNumber.toString();
        shortNumber = shortNumber.substring(0, shortNumber.length() - 2);
        System.out.println(shortNumber + " сотых");
    }
}

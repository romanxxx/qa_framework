package rozetka;

import Common.BaseTest;
import Core.rozetka.RozetkaMainPage;
import Core.rozetka.RozetkaSearchResultPage;
import Core.rozetka.RozetkaWishlistPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class RozetkaTestSuite extends BaseTest {
    @Test
    public void checkAddElementToWishList() {
        final WebDriver webDriver = getDriver();
        final String expectedFirstLinkText = "Панель Speck Presidio Grip для Apple iPhone Xs Max Black/Dark Poppy Red (SP-117106-C305)";
        webDriver.get("https://rozetka.com.ua");
        final RozetkaMainPage rozetkaMainPage = new RozetkaMainPage(webDriver);
        final RozetkaSearchResultPage rozetkaSearchResultPage = rozetkaMainPage.inputSearchGoodsName("apple iphone x s max");
        rozetkaSearchResultPage.selectGood("'Панель Speck Presidio Grip для Apple iPhone Xs Max Black'");
        rozetkaSearchResultPage.clickAddToWishList();
        rozetkaSearchResultPage.inputEmail("rostislavVitas@gmail.com");
        rozetkaSearchResultPage.inputFIO("RostislavVitas Kilob");
        rozetkaSearchResultPage.popupClose();
        final RozetkaWishlistPage rozetkaWishlistPage = rozetkaSearchResultPage.clickOpenWishList();
        final String actualFirstLinkText = rozetkaWishlistPage.getFirstResultWishListText();

        Assert.assertEquals("There is incorrect first link text displayed", expectedFirstLinkText, actualFirstLinkText);


    }
}

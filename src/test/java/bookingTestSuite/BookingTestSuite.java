package bookingTestSuite;

import Common.BaseTest;
import Core.booking.BookingMainPage;
import Core.booking.BookingSearchResultPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class BookingTestSuite extends BaseTest {
    @Test
    public void checkBookingSearch() {
        final WebDriver driver = getDriver();
        driver.get("https://www.booking.com/index.en-gb.html");
        final BookingMainPage bookingMainPage = new BookingMainPage(driver);

        bookingMainPage.searchForMonthAndYear("February 2019");
        bookingMainPage.selectDay("11");
        bookingMainPage.selectDay("23");
        bookingMainPage.typeFromDestination("Stockholm");
        bookingMainPage.clickButtonGuestsCount();
        bookingMainPage.setSelectAdultsCount(2);
        bookingMainPage.setSelectGroupChildren(2);
        bookingMainPage.setSelectNumberRooms(8);
        bookingMainPage.clickFakeSearchButton();
        BookingSearchResultPage resultPage = bookingMainPage.clickButtonSearch();
        final List<String> actualDestinationItems = resultPage.getActualResultDestination();

        final String expectedDateText = "Stockholm";
        for (String unit : actualDestinationItems) {
            final String actualDate = unit.substring(unit.length() - 23, unit.length() - 14);
            Assert.assertEquals("There is no required item in the search result list!", expectedDateText, actualDate);
        }

    }
}

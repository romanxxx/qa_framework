package google;

import Common.BaseTest;
import Core.google.GoogleNamePage;
import Core.google.GoogleSearchResultPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class GoogleTestSuite extends BaseTest {


    @Test
    public void checkGoogleMainPageIsOpened() {
        final WebDriver webDriver = getDriver();
        final String expectedTitle = "Google";
        webDriver.get("https://google.com");

        final String actualTitle = webDriver.getTitle();

        Assert.assertEquals("There is incorrect page title", expectedTitle, actualTitle);


    }

    @Test
    public void checkGoogleSearchFunctionality() {
        final WebDriver webDriver = getDriver();
        final String expectedFirstLinkText = "Компьютерная школа Hillel: курсы IT технологий";

        //1. Открываем главную страницу Google
        webDriver.get("https://www.google.com");

        //2. Создаем обьект главной страницы Google
        final GoogleNamePage page = new GoogleNamePage(webDriver);

        //3. Вводим искоемое слово в поисковик
        final GoogleSearchResultPage googleSearchResultPage = page.typeSearchText("Hillel");

        //4. Возвращаем новую страницу с результатами поиска
        // page.clickSearchButton();

        //5. Получаем текст главной страницы со списком результатов
        final String actualFirstLinkText = googleSearchResultPage.getFirstSearchResultLinkText();

        //6. Сравниваем экспектед текст с ожидаемым текстом
        Assert.assertEquals("There is incorrect first link text displayed", expectedFirstLinkText, actualFirstLinkText);


    }
}

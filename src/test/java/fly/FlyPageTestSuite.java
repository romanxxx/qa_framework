package fly;

import Common.BaseTest;
import Core.fly.FlyPage;
import Core.fly.FlySearchResultPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;


public class FlyPageTestSuite extends BaseTest {
    @Test
    public void CheckDate() throws InterruptedException {

        final WebDriver webDriver = getDriver();
        webDriver.get("https://www.fly.com");
        final FlyPage page = new FlyPage(webDriver);
        page.clickRadiobuttonOneDay();
        page.typeFromText("ABJ");
        page.typeToText("JBC");

        page.clickButtonDateFrom();
        page.searchForMonthAndYear("May", "2019");

        page.selectDay("12");

        FlySearchResultPage resultPage = page.clickButtonSearch();
        resultPage.clickAllButtonMoreDetails();

        final String expectedDateText = "Sun 12 May";
        final List<String> actualDateItems = resultPage.getActualResultDate();


        for (String unit : actualDateItems) {
            final String actualDate = unit.substring(9, 19);
            Assert.assertEquals("There is no required item in the search result list!", expectedDateText, actualDate);
        }
    }
}


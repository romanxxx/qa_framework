package sportcheck;

import Common.BaseTest;
import Core.sportcheck.ProductDetailsPage;
import Core.sportcheck.SportCheckSignAndRegister;
import Core.sportcheck.SportcheckMainPage;
import Core.sportcheck.SportcheckProductDetailsPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class SportChekTestSuite extends BaseTest {

    @Test
    public void checkSportcheckMainPageIsOpened() {
        final String expectedTitle = "Clothes, Shoes & Sporting Gear for Sale Online. Your Better Starts Here. Pre Black Friday Starts November 15. | Sport Chek";
        final WebDriver webDriver = getDriver();
        webDriver.get("https://www.sportchek.ca");

        final String actualTitle = webDriver.getTitle();

        Assert.assertEquals("There is incorrect page title", expectedTitle, actualTitle);


    }

    @Test
    public void checkFilterItems() {
        final WebDriver webDriver = getDriver();

        final String[] expectedFilterItems = {
                "Deals & Features",
                "Men",
                "Women",
                "Kids",
                "Shoes & Footwear",
                "Gear",
                "Electronics",
                "Jerseys & Fan Wear",
                "Sneaker Launches",
                "Shop by Brand",
                "Chek advice"


        };
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage sportcheckMainPage = new SportcheckMainPage(webDriver);
        sportcheckMainPage.expandFilterBox();
        final List<String> actualFilterItems = sportcheckMainPage.getFilterItemsList();
        Assert.assertArrayEquals("There is incorrect page title", expectedFilterItems, actualFilterItems.toArray());


    }

    @Test
    public void checkSportcheckSearchFunctionality() {
        final WebDriver webDriver = getDriver();
        final String expectedFirstLinkText = "Nike Remora Swim Goggles";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        final SportcheckProductDetailsPage sportCheckProductDetailsPage = page.typeSearchText("nike");

        page.clickFirstElement();

        final String actualFirstLinkText = sportCheckProductDetailsPage.getCheckLink();
        Assert.assertEquals("There is incorrect first link text displayed", expectedFirstLinkText, actualFirstLinkText);

    }

    @Test
    public void SportCheckValidationsFieldEmailEmpty() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Email is required.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldConfirmEmail("fdsgds");
        final String actualWarningText = dg.getWarningEmailMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldConfirmEmpty() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Email is required.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("");
        final String actualWarningText = dg.getWarningConfirmEmailMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldEmailAmpersandAbsent() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Please enter your email address in this format: name@example.com";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds");
        final String actualWarningText = dg.getWarningEmailMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldConfirmAmpersandAbsent() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Please enter your email address in this format: name@example.com";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds");
        final String actualWarningText = dg.getWarningConfirmEmailMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldEmailConfirmNoMatch() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Email addresses must match.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.cam");
        final String actualWarningText = dg.getWarningConfirmEmailMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldPasswordEmpty() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Password is required.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldPassword("");
        final String actualWarningText = dg.getWarningPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldConfirmPasswordEmpty() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Password is required.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldPassword("qwerty");
        dg.enterTextIntoFieldConfirmPassword("");
        final String actualWarningText = dg.getWarningConfirmPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldPasswordConfirmPasswordNoMatch() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Passwords must match.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.cam");
        dg.enterTextIntoFieldPassword("qwerty");
        dg.enterTextIntoFieldConfirmPassword("asdfgg");
        final String actualWarningText = dg.getWarningConfirmPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldPasswordLess6() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Your password must be 6-40 characters long.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.cam");
        dg.enterTextIntoFieldPassword("qwert");
        final String actualWarningText = dg.getWarningPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldConfirmPasswordLess6() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Your password must be 6-40 characters long.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.cam");
        dg.enterTextIntoFieldPassword("qwerty");
        dg.enterTextIntoFieldConfirmPassword("a");
        final String actualWarningText = dg.getWarningConfirmPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldPasswordMore40() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Your password must be 6-40 characters long.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.cam");
        dg.enterTextIntoFieldPassword("12345678901234567890123456789012345678901");
        final String actualWarningText = dg.getWarningPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void SportCheckValidationsFieldConfirmPasswordMore40() {
        final WebDriver webDriver = getDriver();
        final String expectedTipText = "Your password must be 6-40 characters long.";
        webDriver.get("https://www.sportchek.ca");
        final SportcheckMainPage page = new SportcheckMainPage(webDriver);
        page.clickSignButton();
        page.clickRegisterNowButton();
        SportCheckSignAndRegister dg = new SportCheckSignAndRegister(webDriver);
        dg.enterTextIntoFieldEmail("fdsgds@ser.com");
        dg.enterTextIntoFieldConfirmEmail("fdsgds@ser.cam");
        dg.enterTextIntoFieldPassword("qwerty");
        dg.enterTextIntoFieldConfirmPassword("12345678901234567890123456789012345678901");
        final String actualWarningText = dg.getWarningConfirmPasswordMessageText();
        Assert.assertEquals("There is incorrect tip text displayed", expectedTipText, actualWarningText);
    }

    @Test
    public void checkMiniCardProductInfo() {
        final WebDriver driver = getDriver();
        driver.get("https://www.sportchek.ca");
        final SportcheckMainPage sportcheckMainPage = new SportcheckMainPage(driver);

        final ProductDetailsPage productDetailsPage = sportcheckMainPage.selectFirstElementFromThePickedJustForYouSection();
        productDetailsPage.selectProductSize(1);
        productDetailsPage.selectProductQty(1);
        productDetailsPage.clickAddToCardButton();
        final String productTitleBeforeAddToCart = productDetailsPage.getProductTitle();
        final String productTitleAfterAddToCart = productDetailsPage.getMiniCartPopupTitle();

        final Integer productQtyBeforeAddToCart = productDetailsPage.getProductQtyBeforeAddToCart();
        final Integer actualProductQty = productDetailsPage.getMiniCartPopupQty();

        Assert.assertEquals("There is incorrect product title displayed!",
                productTitleBeforeAddToCart, productTitleAfterAddToCart);

        Assert.assertEquals("There is incorrect product qty displayed!",
                productQtyBeforeAddToCart, actualProductQty);


    }


}

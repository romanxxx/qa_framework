package Common;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {


    protected WebDriver driver;

    @Before
    public void setDriver() {


        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("-incognito");
//        options.addArguments("--disable-popup-blocking");

//
//        System.setProperty("webdriver.gecko.driver","geckodriver");
//        driver  = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void driverClose() {
        driver.close();
    }

    public WebDriver getDriver() {
        return driver;
    }
}

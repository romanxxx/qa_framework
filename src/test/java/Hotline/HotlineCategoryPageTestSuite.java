package Hotline;

import Common.BaseTest;
import Core.hotline.HotlineAutoPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class HotlineCategoryPageTestSuite extends BaseTest {

    @Test
    public void checkCategoryItems() {
        final WebDriver webDriver = getDriver();

        final String[] expectedCategoryItems = {
                "Зимняя резина",
                "Автозапчасти",
                "Автомобильные аккумуляторы",
                "Антифризы",
                "Автомагнитолы",
                "Видеорегистраторы"
        };
        webDriver.get("https://hotline.ua");
        final HotlineAutoPage hotlineAutoPage = new HotlineAutoPage(webDriver);
        hotlineAutoPage.clickAutoCategory();

        final List<String> actualCategoryItems = hotlineAutoPage.getautoSubElementsList();
        Assert.assertArrayEquals("There is incorrect category items", expectedCategoryItems, actualCategoryItems.toArray());


    }

}
